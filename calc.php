<?php
/**
 * @author   Németh Zoltán <signred@gmail.com>
 * @licence  GNU Public Licence <https://www.gnu.org/licenses/gpl-3.0.en.html>
 * @created  2017. 12. 10. 12:11
 */

include_once 'vendor/autoload.php';

use RS\DueDate\Calculator;

if (array_key_exists('start_date', $_POST) && array_key_exists('turnaround', $_POST)) {
    $calculator = new Calculator();
    $_startDate = new DateTime($_POST['start_date']);
    $calculator->setFreeDays([ Calculator::SATURDAY, Calculator::SUNDAY ])->setDate($_startDate)->setTurnARound($_POST['turnaround'])
        ->calculate();
    echo json_encode(array(
        'start_date' => $calculator->getStartDate()->format(Calculator::FORMAT_FULL),
        'turnaround' => $calculator->getTurnARound(),
        'end_date'   => $calculator->getDate()->format(Calculator::FORMAT_FULL),
    ));
} else {
    echo json_encode([
        'error' => 'Parameters not allowed.',
    ]);
}
